 #  DATTES : **D**ata **A**nalysis **T**ools for **T**ests on **E**nergy **S**torage  

This repository contains the [DATTES](https://gitlab.com/dattes) website and documentation. 

The website is powered by [Hugo](https://gohugo.io/) with the theme [Beautiful Jekyll](https://beautifuljekyll.com/).

## :link: Useful links
- [**DATTES site and documentation**](https://dattes.gitlab.io)
- [**DATTES site and documentation license**](https://gitlab.com/dattes/dattes.gitlab.io/-/blob/master/LICENSE)
- [**DATTES repository**](https://gitlab.com/dattes/dattes)


## :scroll: License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## :sparkles: Contributors

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
      <td align="center">
    <a href="https://mhassini.gitlab.io/">
    <img src="https://cv.archives-ouvertes.fr/photo/882114" height="120px;" alt=""/>
    <br /><sub><b>Marwan Hassini</b></sub></a><br />
        <a href="" title="Created the documentation and site"> <img class="emoji" alt="nrain" src="https://github.githubassets.com/images/icons/emoji/unicode/1f9e0.png?v8" width="20" height="20"> </a>
    <a href="" title="Make documentation, report bug and solve issues"><img class="emoji" alt="book" src="https://github.githubassets.com/images/icons/emoji/unicode/1f4d6.png" width="20" height="20"></a> 
    </td>
    <td align="center">
    <a href="https://cv.archives-ouvertes.fr/redondo">
    <img src="https://cv.archives-ouvertes.fr/photo/326135" height="120px;" alt=""/>
    <br /><sub><b>Eduardo Redondo-Iglesias</b></sub></a><br />
    <a href="" title="Make documentation, report bug and solve issues"><img class="emoji" alt="book" src="https://github.githubassets.com/images/icons/emoji/unicode/1f4d6.png" width="20" height="20"></a> 
    </td>
    

  </tr>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->
<!-- A complete list of emoji may be found here : https://datasette-graphql-demo.datasette.io/github/emojis  -->
This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!


