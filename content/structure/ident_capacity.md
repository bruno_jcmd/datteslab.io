---
title : ident_capacity
date: 2022-05-04
---

---
# Capacity

## 1-Definition of capacity
The capacity  of a cell, expressed in Ampere-hours (Ah), is the amount of charge the cell can deliver at a given constant current. It varies according to the electrode materials and the internal design of the cell. It tends to decrease during the life of the cell. [Source: [Romain Mathieu 2020](https://tel.archives-ouvertes.fr/tel-02920329)]

## 2-Structure in DATTES
![capacite.svg](uploads/11504d2fda316004820a53ecc66a07c5/capacite.svg)
To be replaced by a more relevant tree structure

## 3- Method
The capacity is calculated by summing the capacity calculated during the DC phase with that of a possible CV phase.

The **capacity** is calculated by the ident_capacity function with the following code:

```
%CC part
Capa = abs([phases(config.pCapaD | config.pCapaC).capacity]);
Regime = [phases(config.pCapaD | config.pCapaC).Iavg]./config.Capa;

%CV part
phasesCV = phases(config.pCapaDV | config.pCapaCV);
CapaCV = abs([phases(config.pCapaDV | config.pCapaCV).capacity]);
dCV = [phasesCV.duration];
UCV = [phasesCV.Uavg];

```
## 4-Key quantities for the calculation
The four key quantities for the calculation of the capacity are :
- **config.pCapaD**, 
- **config.pCapaC**,
- **config.pCapaDV**,
- **config.pCapaCV**.


They are determined in configurator :
```
%1) phases capa decharge (CC):
%1.1. imoy<0
%1.2 - end at SoC0 (I0cc)
%1.3 - are preceded by a rest phase at SoC100 (I100r or I100ccr)
pCapaD = [phases.Imoy]<0 & ismember(tFins,t(I0cc)) & [0 pRepos100(1:end-1)];
%2) phases capa charge (CC):
%2.1.- Imoy>0
%2.2 - end at SoC100 (I100cc)
%2.3 - are preceded by a rest phase at SoC0 (I0r or I0ccr)
pCapaC = [phases.Imoy]>0 & ismember(tFins,t(I100cc)) & [0 pRepos0(1:end-1)];
% pCapaC = [phases.Imoy]>0 & [0 pRepos0(1:end-1)];%LYP, BRICOLE
%3) residual load phases
%1.1.- Imoy<0
%1.2 - end at SoC0 (I0)
%1.3 - are preceded by a phase pCapaD
pCapaDV = [phases.Imoy]<0 & ismember(tFins,t(I0)) & [0 pCapaD(1:end-1)];
%4) residual load phases
%1.1 - Imoy>0
%1.2 - end at SoC100 (I100)
%1.3 - are preceded by a phase pCapaC
pCapaCV = [phases.Imoy]>0 & ismember(tFins,t(I100)) & [0 pCapaC(1:end-1)];
```


## 5-Assumptions and possible simplifications
No major assumptions or simplifications have been made

