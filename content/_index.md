DATTES is a software which have been developed in order to facilitate and accelerate data processing in the energy storage field.
It is written in MATLAB and all functions are GNU Octave compatible

DATTES makes possible to:
- Read experimental results file from a wide range of battery cyclers
- Convert proprietary results file format to open and standards ones
- Process experimental data to determine main features including low current behavior or electrochemical impedance spectroscopy
- Visualize experimental and processed data 
- Model experimental data

## Code repository
The different DATTES stable versions are [**here**](https://gitlab.com/dattes/dattes/-/releases)

The current DATTES development version is [**here**](https://gitlab.com/dattes/dattes)


## Site content
- [**Learn the DATTES workflow**](/page/documentation/dattes_workflow/)
- [**Getting started with DATTES**](/page/documentation/getting_started/)
- [**Tutorials**](/page/documentation/tutorials/)
- [**Examples**](/page/examples/examples)
- [**FAQ Users**](/page/faqusers/)
- [**How to contribute to DATTES ?**](/page/contribute/)
- [**Updates about releases and publications**](/news)
- [**Contact page**](/page/contact)
	
## Licence
DATTES is free software licensed under GNU GPL v3.


Copyright (c) 2015-2023, E. Redondo, M. Hassini (Univ. Gustave Eiffel).

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.
You should have received a copy of the GNU General Public
License along with this program. If not, see
<https://www.gnu.org/licenses/>.



## Authors
DATTES have been created in 2015 by :
- [Eduardo Redondo-Iglesias](https://cv.hal.science/redondo) from [University Gustave Eiffel](https://www.univ-gustave-eiffel.fr/).

Main contributors to the code and the documentation are :
- [Eduardo Redondo-Iglesias](https://cv.hal.science/redondo)  from [University Gustave Eiffel](https://www.univ-gustave-eiffel.fr/).
- [Marwan Hassini](https://cv.hal.science/marwan-hassini) from [University Gustave Eiffel](https://www.univ-gustave-eiffel.fr/).

## Citing DATTES
If you use DATTES in your work, please cite our paper :

> Eduardo Redondo-Iglesias, Marwan Hassini, Pascal Venet and Serge Pelissier,
DATTES: Data analysis tools for tests on energy storage,
SoftwareX,
Volume 24,
2023,
101584,
ISSN 2352-7110,
https://doi.org/10.1016/j.softx.2023.101584.
(https://www.sciencedirect.com/science/article/pii/S2352711023002807)

You can use the bibtex : 
```
@article{redondo_iglesias_eduardo_2023_8134473,
  title = {DATTES: Data analysis tools for tests on energy storage},
journal = {SoftwareX},
volume = {24},
pages = {101584},
year = {2023},
issn = {2352-7110},
doi = {https://doi.org/10.1016/j.softx.2023.101584},
url = {https://www.sciencedirect.com/science/article/pii/S2352711023002807},
author = {Eduardo Redondo-Iglesias and Marwan Hassini and Pascal Venet and Serge Pelissier},
keywords = {Energy storage, Experiments, Matlab, GNU octave, Data analysis, Open science, FAIR}
}
```