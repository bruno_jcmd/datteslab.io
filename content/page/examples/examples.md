 ---
title: Examples
subtitle: Learning to use DATTES by examples
comments: false
---

# Selection of studies using DATTES
To demonstrate the capability of DATTES, a selection of public dataset have been processed with DATTES.

- [**DATTES demo**](/page/examples/demo_dattes)
- [**Second life battery databank**](/page/examples/second_life)
- [**CALCE databank**](/page/examples/calce)





