---
title: FAQ users
subtitle: Frequently Asked Questions - Using DATTES
comments: false
---
 
# Outline
- [How to ask a question ?](#ask-a-question)
- [Frequently Asked Questions from users](#frequently-asked-questions-from-users)
     - [What is the DATTES license ?](#dattes-license)
     - [How to cite DATTES ?](#citing-dattes)
     - [My battery cycler can't be analyzed with DATTES](#add-battery-cyclers-to-dattes)



# Ask a question
For any question regarding the use of DATTES, you can email dattes@univ-eiffel.fr


# Frequently Asked Questions from users

## DATTES license
DATTES is a free software, licensed under [GNU GPL v3](https://www.gnu.org/licenses/quick-guide-gplv3.html)


```
Copyright (c) 2015, E. Redondo, M. Hassini.

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.
You should have received a copy of the GNU General Public
License along with this program. If not, see
<https://www.gnu.org/licenses/>.
```


## Citing DATTES 

If you use DATTES in your work, please cite our paper :

> Eduardo Redondo-Iglesias, Marwan Hassini, Pascal Venet and Serge Pelissier,
DATTES: Data analysis tools for tests on energy storage,
SoftwareX,
Volume 24,
2023,
101584,
ISSN 2352-7110,
https://doi.org/10.1016/j.softx.2023.101584.
(https://www.sciencedirect.com/science/article/pii/S2352711023002807)

You can use the bibtex : 
```
@article{redondo_iglesias_eduardo_2023_8134473,
  title = {DATTES: Data analysis tools for tests on energy storage},
journal = {SoftwareX},
volume = {24},
pages = {101584},
year = {2023},
issn = {2352-7110},
doi = {https://doi.org/10.1016/j.softx.2023.101584},
url = {https://www.sciencedirect.com/science/article/pii/S2352711023002807},
author = {Eduardo Redondo-Iglesias and Marwan Hassini and Pascal Venet and Serge Pelissier},
keywords = {Energy storage, Experiments, Matlab, GNU octave, Data analysis, Open science, FAIR}
}
```


We would be grateful if you could also cite some of ours relevant papers.

The complete list of our publications is available [here](https://cv.archives-ouvertes.fr/redondo).

## Add battery cyclers to DATTES
If your battery cycler can not be analyzed with DATTES, please :
1. Write an email to dattes@univ-eiffel.fr or create an issue in DATTES : [Link](https://gitlab.com/dattes/dattes/-/issues) 
2. Provide an example of your battery cycler data

We will try to make your cycler compatible with DATTES.
