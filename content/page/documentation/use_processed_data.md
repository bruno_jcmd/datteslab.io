  ---
title: Use processed data
subtitle: 
comments: false
---
The section [**Import cycler files**](/page/documentation/import_files) have explained how to convert an experimental file from a proprietary format into a standard one.

The section [**Structure test files for the analysis**](/page/documentation/structure_files) have explained how to structure the experimental file to facilitate the analysis. 

The section [**Configuration test files for the analysis**](/page/documentation/create_configuration) have explained how to configure the main features analysis.

The section [**Analyse main features**](/page/documentation/analyze_results) have explained how to process the main features analysis.

This section describes how the process data can be used for modeling or prediction studies. To do so, two tutorials and a selection of studies using data processed thanks to DATTES are shared :

- [**Export results to other file formats**](/page/documentation/export_results)
- [**Couple DATTES with other open softwares**](/page/documentation/other_softwares)
- [**Selection of studies using processed data by DATTES**](/page/examples/examples/)
