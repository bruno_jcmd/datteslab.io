---
title: Segment test 
subtitle:  Segment experimental data into profiles,modes and phases
comments: false
---

## Profiles, phases and modes
To facilitate and accelerate the analysis, DATTES structures an experimental file according to three elements : the profiles, the modes and the phases. 

**Profiles** are the vectors containing the most important physical quantities for an energy storage system : the time t, the voltage U, the current I and the temperature T.

**Modes** are the categories used for classifying experimental points according to the classic motif found in the energy storage field. Five different modes have been defined. Mode 1 is used for experimental point belonging to a constant current motif. Motif 2 is for the ones belonging to a constant voltage motif. Motif 3 is for rest experimental points. Motif 4 is used for experimental points belonging to an electrochemical impedance spectroscopy motif. Finally, motif 5 is used to identify a usage profile such as a WLTC profile.

**Phases** are identifying the different motifs of a test, that is to say the portions where the mode is constant.

## How to use DATTES for structuring data ?

DATTES will structure your data if you run `dattes_import` :

```
result = dattes_import(file_in, options, destination_folder, read_mode)

% dattes_import - DATTES Import function
% 
% This function read .xml (or .json, or .csv) files, read metadata files
% (.meta) and performs some basic calculations (which_mode, split_phases,
% calcul_soc). The results are given as output and can be stored in mat
% files.
%
% Usage:
% result = dattes_import(file_in)
% - read single file (json, csv, or xml), default options
% result = dattes_import(file_in, 's')
% - read single file, MAT file will be saved beside file_in (same pathname, different extension)
% result = dattes_import(file_in,options,destination_folder)
% - read single file, MAT file will be saved in destination_folder
% result = dattes_import(file_list,...)
% - read each file in file_list, result is [mx1 cell struct]
% result = dattes_import(source_folder,options,destination_folder, read_mode)
% - read each file in source_folder result is [mx1 cell struct],
% read_mode must be specified ('json', 'csv' or 'xml')
%
% Inputs:
% - file_in [1xp string]: filename to read , or
% - file_list [mx1 cell string]: file list to read, or
% - source_folder [1xp string]: source folder to search files to read
% - options [1xn string]:
%    - 'S': run calcul_soc (default if no soc vector is in file_in)
%    - 'm': run which_mode (default if no mode vector is in file_in)
%    - 'v': verbose, tell what you do
%    - 's': save result(s) in mat file(s)
%    - 'f': force, read file_in even if mat file exists, otherwise read mat file instead
%    - 'u': update, read if mat file exists but is older than file_in
% - destination_folder [1xp string]: folder to store mat files.
%     If not given, mat files will be stored beside file_in
% - read_mode [1x3 or 1x4 string]: needed if source_folder, optional if not
%    - xml: read xml files
%    - json: read json files
%    - csv: read csv files
% Output:
% - result [1x1 struct] DATTES result structure
% - result [mx1 cell struct] DATTES result cell structure if multiple files read
%
% See also dattes_export
%
% Copyright 2015 DATTES_Contributors <dattes@univ-eiffel.fr> .
% For more information, see the <a href="matlab: 
% web('https://gitlab.com/dattes/dattes/-/blob/main/LICENSE')">DATTES License</a>.

```


## What are the functions behind data structure ?

`dattes_import` relies on three important functions :
 - [**extract_profiles**](https://gitlab.com/dattes/dattes/-/blob/main/data_tools/extract_profiles.m)
 - [**which_mode**](https://gitlab.com/dattes/dattes/-/blob/main/data_tools/which_mode.m)
 - [**split_phases**](https://gitlab.com/dattes/dattes/-/blob/main/data_tools/split_phases.m)
