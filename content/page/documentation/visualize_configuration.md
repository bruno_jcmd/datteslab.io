--- 
title : Visualize configuration
---
# Outline
- [**Code for analysis**](#code-for-analysis)
- [**Code for visualization**](#code-for-visualization)
- [**Methodology and hypothesis**](#methodology-and-hypothesis)
- [**Contribute to configuration analysis**](#contribute-to-configuration-analysis)



## Code for analysis

To analyze the test configuration, the action 'c' should be used :

`[result] = dattes(XML_file,'cvs','cfg_file');`


## Code for vizualization
To visualize the test configuration, the action 'Gc' should be used :

`[result] = dattes(XML_file,'Gc');`

The graph should look like

![image](/images/figure_Gc.png)
![image](/images/figure_Gc_2.png)
![image](/images/figure_Gc_3.png)



## Methodology and Hypothesis
### Method
The configuration analysis is done systematically. It is one of the first step in the analysis of data.
`config = configurator(t,U,I,m,config,phases,options)` is the function responsible for the analysis of the configuration file.

### Key quantities for the calculation
There are no calculations made. The function `configurator` used the data from the XML file which creation is explained in the tutorial [**Import cycler files**](/page/documentation/import_files/) and the configuration file which creation is explained in the tutorial [**Create a configuration file**](/page/documentation/create_configuration/).


### Assumptions and possible simplifications
No major assumptions or simplifications have been made

## Contribute to configuration analysis
A list of open issues related to profiles analysis and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues/?search=config&sort=created_date&state=opened&first_page_size=20).

 


 
