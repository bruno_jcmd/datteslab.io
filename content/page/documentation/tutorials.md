 ---
title: Tutorials
subtitle: Learning to use DATTES thanks to tutorials
comments: false
---
Tutorials may help you getting started with DATTES by giving you a step-by-step recipe.
For the novice users, it is recommended to follow the sequence presented in the list below.

Some public datasets have also been analysed with DATTES to provide examples of DATTES use.
These analysis are available on the page [**Examples**](/page/examples/examples).


# Tutorial list
- [**Import cycler files**](/page/documentation/import_files)
- [**Structure test files for the analysis**](/page/documentation/structure_files)
	- [**Write and use metadata**](/page/documentation/write_metadata)
	- [**Segment experimental data into profiles, modes and phases**](/page/documentation/profiles_modes_phases)
- [**Configure test files for the analysis**](/page/documentation/configuration_files)
	- [**Define analysis methodology**](/page/documentation/create_configuration)
	- [**Process anomaly detection**](/page/documentation/anomaly_detection)
- [**Analyse main features**](/page/documentation/analyze_results)
	- [**Capacity**](/page/documentation/analyze_capacity)
	- [**State of charge**](/page/documentation/analyze_soc)
	- [**Resistance**](/page/documentation/analyze_resistance)
	- [**Impedance**](/page/documentation/analyze_impedance)
	- [**Open Circuit Voltage**](/page/documentation/analyze_ocv)
	- [**Electrochemical Impedance Spectroscopy**](/page/documentation/analyze_eis)
	- [**Incremental Capacity Analysis**](/page/documentation/analyze_ica)
- [**Visualize main features**](/page/documentation/dattes_plot_gallery)
- [**Use processed data**](/page/documentation/use_processed_data)
	- [**Export results to other file formats**](/page/documentation/export_results)
	- [**Couple DATTES with other open softwares**](/page/documentation/other_softwares)
	- [**Selection of studies using processed data by DATTES**](/page/examples/examples/)
