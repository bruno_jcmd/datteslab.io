 --- 
title : Resistance
---
# Outline
- [**Resistance definition**](#resistance-definition)
- [**Analysis preparation**](#analysis-preparation)
- [**Anomaly detection**](#anomaly-detection)
- [**Resistance analysis**](#resistance-analysis)
- [**Result visualization**](#result-visualization)
- [**Methodology and hypothesis**](#methodology-and-hypothesis)
- [**Contribute to resistance analysis**](#contribute-to-resistance-analysis)
## Resistance definition
The  resistance is the opposition to alternating current presented by the combined effect of resistance and reactance in a circuit. Commonly, in a battery it is defined by the voltage drop when a current is applied. 

$$ R=  { \Delta U \over \Delta I }$$
with 
- R : the resistance (Ω)
- ΔU : the voltage variation (V)
- ΔI : the current variation (A)


## Analysis preparation
DATTES is called as follows : `[result]=dattes(XML_file,'action','configuration_file')`. 
Before any analysis, it is then necessary to create the XML and configuration files.

The section [**Import cycler files**](/page/documentation/import_files) explains how to create the XML file. 

The section [**Create a configuration file**](/page/documentation/create_configuration) explains how to create a configuration file.

## Anomaly detection

Resistance tests may be affected by poor detection of a measurement as well as noisy current and voltage measurements. 

To check if an impedance measurement have been detected by DATTES, the action 'c' should be used : `[result] = dattes(XMLfile,'cvs');`and plotted :`[result] = dattes(XMLfile,'Gc');`


The impedance measurement detected according to the configuration provided are highlighted as in the following image :

![image](/images/normal_impedance_detection_test.png)


To check if an impedance test have run normally the action 'gR' should be used :  `[result] = dattes(XMLfile,'gRvs');`


Definitions of noisy current and voltage measurements should be adapted to the cycler accuracy. In the following image, a normal impedance measurement for a cycler with a 1 mV accuracy is presented :

![image](/images/normal_impedance_test.png)



## Resistance analysis

To analyze the resistance, the action 'R' should be used :

`[result] = dattes(XML_file,'Rvs');`

The output are :

| Output structure |Field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |:--- |
| result |  resistance|  `R`| Ohm|Resistance value|
| result | resistance |  `dod`| Ah|DoD of resistance measurement|
| result | resistance |  `crate`| - |Current rate of resistance measurement|
| result | resistance | `time` | s|Instant of resistance measurement in the test time|
| result | resistance | `delta_time` |  s|Instant of resistance measurement in the pulse time|


## Code for visualization
To visualize the resistance, the action 'GR should be used :

`[result] = dattes(XML_file,'GR');`

The graph should look like

![image](/images/figure_GR.png)




## Methodology and Hypothesis
### Method
For each pulse considered for resistance calculation, the different arrays are determined in the function `calcul_r`.

The array `result.resistance.R` is determined as follows : 

```
w = abs(tpulse-tpulse(end));  %polynominal interpolation weight for pulse, points away from current impulse have a higher weight

Ural_pul = fitting_pol2(tpulse,Upulse,instant_end_rest+t_calcul_R(ind),w);
%Voltage extrapolation for the pulse, the point at the pulse instant is extrapolated

w = abs(trepos-trepos(1));polynominal interpolation weight for rest, points away from current impulse have a higher weight

Ural_rep=fitting_pol2(trepos,Urepos,instant_end_rest+t_calcul_R(ind),w);
 %Voltage extrapolation for the rest, the point at the pulse instant is extrapolated
            
R = (Ural_pul-Ural_rep)/R_I(end); %the resistance is the difference of potential between rest and pulse divided by current l
```
The following image presents an exemple of the points used for a resistance measurement
![image](/images/methodology_resistance.png)

The array `result.resistance.crate` is determined as follows : 
```
R_crate= mean(Ipulse);  %resistance estimation current is the average of the current
```

The array `result.resistance.time` is determined as follows : 
```
Rt= t(1) + duration_repos; %resistance estimation time is the instant at the end of rest
```

The array `result.resistance.dod` is determined as follows : 
```
RDoD= DoDAh(1); % resistance DoD is the DoD at rest
```

The array `result.resistance.delta_time` is determined as follows : 
```
Rdt = t_calcul_R(ind); % Resistance delta time is determined by user in the configuration file
```


### Key parameters for the calculation
The four key parameters for the calculation of the resistance are :
- **config.minimal_duration_rest_before_pulse**, 
- **config.minimal_duration_pulse**,
- **config.instant_end_rest**,
- **config.pR**.


### Assumptions and possible simplifications
No major assumptions or simplifications have been made

## Contribute to resistance analysis
A list of open issues related to resistance calculation and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues/?search=resistance&sort=created_date&state=opened&first_page_size=20)


