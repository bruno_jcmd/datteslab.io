 --- 
title : Incremental Capacity Analysis
---
# Outline
- [**Incremental Capacity Analysis definition**](#incremental-capacity-analysis-definition)
- [**Analysis preparation**](#analysis-preparation)
- [**Anomaly detection**](#anomaly-detection)
- [**Incremental Capacity Analysis analysis**](#incremental-capacity-analysis-analysis)
- [**Result visualization**](#result-visualization)
- [**Methodology and hypothesis**](#methodology-and-hypothesis)
- [**Contribute to Incremental Capacity Analysis analysis**](#contribute-to-ica-analysis)

## Incremental Capacity Analysis definition
The  impedance is the opposition to alternating current presented by the combined effect of resistance and reactance in a circuit. Commonly, in a battery it is defined by the voltage drop when a current is applied. 

$$ ICA=  { \Delta Q \over \Delta V }$$
with 
- ICA : the Incremental Capacity Analysis (Ah/V)
- ΔQ : the capacity variation (Ah)
- ΔV : the voltage variation (V)


## Analysis preparation
DATTES is called as follows : `[result]=dattes(XML_file,'action','configuration_file')`. 
Before any analysis, it is then necessary to create the XML and configuration files.

The section [**Import cycler files**](/page/documentation/import_files) explains how to create the XML file. 

The section [**Create a configuration file**](/page/documentation/create_configuration) explains how to create a configuration file.

## Anomaly detection

Incremental Capacity Analysis tests may be affected by noisy voltage measurements. 

To check if an Incremental Capacity Analysis test have run normally the action 'GI' should be used :  `[result] = dattes(XMLfile,'GI');`


![image](/images/normal_ica_test.png)




## Incremental Capacity Analysis analysis

To analyze the incremental capacity analysis, the action 'I' should be used :

`[result] = dattes(XMLfile,'Ivs');`

The output are :

| Output structure | Field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |:--- |
| result |  ica|  `dqdu`| Ah/V| Voltage derivative of capacity|
| result | ica |  `dudq`| V/Ah| Capacity derivative of voltage|
| result | ica |  `q`| Ah | Capacity vector for dudq |
| result | ica | `u` | V | Voltage vector for dqdu |
| result | ica | `crate` | - | Current rate for the ica test|
| result | ica | `time` | s | Time of measurement of the ica test|


## Code for vizualization
To visualize the resistance, the action 'GI' should be used :

`[result] = dattes(XMLfile,'GI');`

The graph should look like

![image](/images/figure_GI.png)


## Methodology and Hypothesis
### Method
All the array from `result.ica` are determined thanks to function `extract_phase` :
```
for ind = 1:length(phasesICA)
    [tp,Up,DoDAhp] = extract_phase(phasesICA(ind),t,U,DoDAh);
    
    [ica(ind).dqdu, ica(ind).dudq, ica(ind).q, ica(ind).u] = calcul_ica(tp,DoDAhp,Up,N,wn,f_type);
    ica(ind).crate = phasesICA(ind).Iavg/config.Capa;
    ica(ind).time = phasesICA(ind).t_fin;
end
```

### Key quantities for the calculation
The six key parameters for the analysis of ica are :
- config.pICAC,
- config.pICAD,
- config.n_filter,
- config.wn_filter,
- config.filter_type,
- config.Capa.



### Assumptions and possible simplifications
No major assumptions or simplifications have been made

## Contribute to ica analysis
A list of open issues related to ica calculation and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues/?search=ica&sort=created_date&state=opened&first_page_size=20).


