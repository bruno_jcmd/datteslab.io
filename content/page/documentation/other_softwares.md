 --- 
title : Couple DATTES with other open softwares
---
DATTES have been designed for facilitating and accelerating research in the energy storage field. If data processing is at the cornerstone of all energy storage studies, modeling and simulations studies are often the final purpose. DATTES can be coupled with some of the open modeling and simulations softwares in the field.

# VEHLIB
Vehlib software is a simulation tool for energy management in electric vehicles. It is based on a block diagram description associated with principles from Bond-Graph theory. A Backward approach proposes energy management strategy based on DP and PMP principles.

VEHLIB is available [here](https://gitlab.univ-eiffel.fr/eco7/vehlib) under license GNU GLP V3.

## How to use DATTES data in VEHLIB ?
(To be written...)