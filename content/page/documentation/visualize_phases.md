--- 
title : Visualize phases
---
# Outline
- [**Code for analysis**](#code-for-analysis)
- [**Code for visualization**](#code-for-visualization)
- [**Methodology and hypothesis**](#methodology-and-hypothesis)
- [**Contribute to phases analysis**](#contribute-to-phases-analysis)



## Code for analysis

To analyze the test phases, the action 'c' should be used :

`[result] = dattes(XML_file,'cvs','cfg_file');`

## Code for vizualization
To visualize the test phases, the action 'Gp' should be used :

`[result] = dattes(XML_file,'Gp');`

The graph should look like

![image](/images/figure_Gp.png)
![image](/images/figure_Gp_I.png)



## Methodology and Hypothesis
### Method
The split of the test profile into phases is done systematically. It is one of the first step in the analysis of data.
`[phases, tcell, Icell, Ucell, modes] = split_phases(t,I,U,m,options)`is the function responsible for the split of the test profiles into phases.
### Key quantities for the calculation
There are no calculations made


### Assumptions and possible simplifications
No major assumptions or simplifications have been made

## Contribute to phases analysis
A list of open issues related to phases analysis and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues/?search=phases&sort=created_date&state=opened&first_page_size=20).
