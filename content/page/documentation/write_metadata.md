---
title: Write and use metadata
subtitle: 
comments: false
---
## What is metadata ?
Metadata is data that provides context and information about the processed experimental data. Comprehensive metadata for experimental data is at the cornerstone of the FAIR Principles which aim to ensure research data are findable, accessible, interoperable, and reusable. 

DATTES aims to facilitate the metadata sharing in the energy storage field by providing tools for experimentators in order to ease metadata creation and sharing. The comprehensive metadata structure proposed in DATTES should help energy storage data analyst to analyse and reuse high quality data.

## How metadata is structured in DATTES ?
Metadata in DATTES is structured in two levels (_sections_ and _fields_).
At the first level there are five _sections_:
**test**, **cell**, **cycler**, **chamber** and **regional**.
Each _section_ has some _fields_ that can be text or numeric.
There are no mandatory sections or fields.
The full metadata structure is described below:
1. **test**: general information about the test
    - **institution** (text): University or Center name
    - **laboratory** (text): Laboratory name
    - **experimenter** (text): person in charge of tests
    - **datetime** (text): date and time of test
    - **temperature** (numeric): ambient temperature (Celsius)
    - **purpose** (text): brief description of the test
1. **cell**: general information about the cell under test
    - **id** (text): cell's unique identifier
    - **brand** (text): manufacturer or brand
    - **model** (text): cell model
    - **max_voltage** (numeric): maximum voltage
    - **min_voltage** (numeric): minimum voltage
    - **nom_voltage** (numeric): nominal voltage
    - **nom_capacity** (numeric): nominal capacity (Ah)
    - **max_dis_current_cont** (numeric): maximum continuous discharging current (A)
    - **max_cha_current_cont** (numeric): maximum continuous charging current (A)
    - **min_temperature** (numeric): minimum cell operating temperature (Celsius)
    - **max_temperature** (numeric): maximum cell operating temperature (Celsius)
    - **geometry** (text): cell format (pouch, prismatic or cylindrical)
    - **dimensions** (numeric array): LxWxH if pouch or prismatic, DxL if cylindrical (mm)
    - **weight** (numeric): cell weight (grams)
    - **cathode** (text): cell cathode materials
    - **anode** (text): cell anode materials
1. **cycler**: general information and performance of the used cycler 
    - **brand** (text): cycler brand
    - **model** (text): cycler model
    - **voltage_resolution** (numeric): voltage measurement resolution (V)
    - **current_resolution** (numeric): current measurement resolution (A)
    - **temperature_resolution** (numeric): current measurement resolution (Celsius)
    - **min_voltage** (numeric): minimum cycler voltage
    - **max_voltage** (numeric): maximum cycler voltage
    - **max_discharging_current** (numeric): maximum cycler discharging current
    - **max_charging_current** (numeric): maximum cycler charging current
    - **cell_voltage_name** (text): variable name for cell voltage measurement
    - **cell_temperature_name** (text): variable name for temperature measurement
1. **chamber**:  general information and performance of the used climatic chamber 
    - **brand** (text): chamber brand
    - **model** (text): chamber model
    - **min_temperature_capability** (numeric): minimum temperature (Celsius)
    - **max_temperature_capability** (numeric): maximum temperature (Celsius)
1. **regional**: regional settings (no yet used)
    - **date_format** (text): string specifying date format
    - **time_format** (text): string specifying time format

**NOTE:** Some metadata may help DATTES to perform better.
For example, **max_voltage**, **min_voltage** and **nom_capacity** can be used to detect full charges / full discharges, SoC references, etc.
In many cases, you can add this information to avoid the need for configuration scripts.


## How metadata is shared ?
Metadata are stored in JSON formatted text, file extension is **.meta**.


## How metadata is read ?
Metadata files are systematically processed by DATTES during data preparation for analysis steps, more specifically during the second step of the DATTES workflow: [structure files for analysis](/page/documentation/structure_files).

The main function behind this operation is `metadata_collector`, its role is to collect and make a synthesis of all the **.meta** files which are in the file structure of the processed experimental file.

Once all the **.meta** files are collected, their content is read and a synthesis of the metadata in the file structure is made. A _bottom over top_ priority is given to metadata fields, meaning that if two metadata files provides incompatibles information in a specific field it is the one in the subfolder which will be considered.

### Example
Consider the following file structure:
```
 [drwx------]  battery_tests
├── [drwx------]  inr18650
├── [-rwx------]  inr18650.meta
│   ├── [drwx------]  checkup_tests
│   ├── [-rwx------]  checkup_tests.meta
│   │   ├── [drwx------]  cell1
│   │   ├── [-rwx------]  cell1.meta
│   │   │   ├── [-rwx------]  20190102_1230_initial_checkup.csv
│   │   │   ├── [-rwx------]  20190102_1230_initial_checkup.meta
│   │   │   ├── [-rwx------]  20190202_1230_intermediary.csv
│   │   │   ├── [-rwx------]  20190202_1230_intermediary.meta
│   │   │   ├── [-rwx------]  20190302_1230_intermediary.csv
│   │   │   ├── [-rwx------]  20190302_1230_intermediary.meta
│   │   │   ├── [-rwx------]  20190402_1230_final.csv
│   │   │   └── [-rwx------]  20190402_1230_final.meta
...
│   │   ├── [drwx------]  cell2
│   │   ├── [-rwx------]  cell2.meta
...
│   │   ├── [drwx------]  cell3
│   │   └── [-rwx------]  cell3.meta
...
│   ├── [drwx------]  cycling_tests
│   │   ├── [drwx------]  cell1
│   │   │   ├── [-rwx------]  20190103_1230_cycling.csv
│   │   │   └── [-rwx------]  20190103_1230_cycling.meta
│   │   ├── [drwx------]  cell2
...
│   │   └── [drwx------]  cell3
...
│   └── [-rwx------]  cycling_tests.meta
├── [drwx------]  lfp26650
├── [-rwx------]  lfp26650.meta
```
In the example above, data is organised as follows:
- first level: battery technology (subfolder inr18650 or lfp26650)
- second level: test type (subfolder checkup_tests or cycling_tests)
- third level: cell number (subfolder cell1, cell2 or cell3)
- fourth level: data (**.csv** files)

When DATTES is used to process **battery_tests/inr18650/checkup_tests/cell1/20190302_1230_intermediary.csv**,
`metadata_collector` will look for the following metadata files :
1. **battery_tests.meta**
2. **battery_tests/inr18650.meta**
3. **battery_tests/inr18650/checkup_tests.meta**
4. **battery_tests/inr18650/checkup_tests/cell1.meta**
5. **battery_tests/inr18650/checkup_tests/cell1/20190302_1230_intermediary.meta**

The metadata files will be processed according to the above sequence and for each metadata file, if a field correspond to an already existing one, it will be processed and will overwrite the existing field.
In this way, common metadata for a family of tests can be written just once, at the corresponding level.

For example
- **battery_tests** may contain institution and laboratory names.
- **inr18650.meta** may contain general characteristics of the cell, cycler, etc.
- **checkup_tests** may contain test purpose, ambient temperature, etc.
- **20190302_1230_intermediary.meta** may contain test experimenter, and other details

Of course, all metadata could be included just at the last level (e.g. **20190302_1230_intermediary.meta**).
But in this case, all information must be written in each **.meta** file beside each **.csv** file.
It's up to the person in charge of writting metadata to choose which option is more convenient.

## How to write metadata files ?

### Method 1 : The personalised structure 
You can manually create a metadata file with the comprehensive structure described above.
To adapt the structure to your needs you can edit the `metadata_example` script and uncomment/modify some lines:
```
edit metadata_example.m
```

You can then save this script with a new name and run it to get a **metadata** structure in the current workspace.

Finally, you can save this metadata in a **.meta** file with `metadata_json_export`:
```
metadata_json_export('your_filename.meta', metadata)
```

As described above, you may choose to write some metadata concerning a folder (family of tests) or a file (individual test).
Then your **.meta** file should be placed beside a folder or a file with the same same (excluding extensions).

### Method 2 : The DATTES structure 
You can use `metadata_form`.
```
metadata_form
```
This script will ask you some questions about your data in four steps (test, cell, equipment, other) .
At the beginning of each section (test, cell, equipment, other) you can skip the section  by answering "No" ("N").
Inside each section, you fill each field or you can skip them (empty answers mean ignored fields).

After that, you will be asked to save the created metadata struct in a **.meta** file.
If you choose so, you can choose either to apply metadata to a whole folder, or to a single file.
A **.meta** file with the same name of the chosen folder or file will be created.
This will help to keep a consistent _data+metadata_ structure for `metadata_collector` as described above.

Otherwise, if you do not choose to save the metadata into a **.meta** file,
you can get the variable **metadata** in the current workspace,
for verification and/or modification  before saving it.

Finally, you can manually save this metadata in a **.meta** file with `metadata_json_export` (in the same way of method 1):
```
metadata_json_export('your_filename.meta', metadata)
```

