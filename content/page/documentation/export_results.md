---
title: Export results
subtitle: 
comments: false
---

## Introduction

By default, DATTES results are stored in *.mat files, each containing a result structure composed of the following fields :
- **profiles**
- **eis**
- **phases**
- **metadata**
- **configuration**

This format may not be suitable for further use with other software. To ease softwares interoperability, the function **dattes_export** have been developed.
This function enable export of the DATTES processed data into other file formats such as **.csv** or **.json**

This function **dattes_export**. is called as follows : `dattes_export(dattes_struct, options, file_out)`
The following sections describe how to export each input of **dattes_export**.

## dattes_struct
### Profiles
**DATTES profiles** structure contains time dependent vectors (measurements).
Generally, the profiles struct contains the following vectors:
- **t** (nx1 double): test time from 1/1/2000 (s)
- **U** (nx1 double): cell voltage (V)
- **I** (nx1 double): cell current (A)
- **m** (nx1 double): cycler mode (1 = CC, 2=CV, 3 = rest, 4 = EIS, 5 = random profile)
- **T** (nx1 double): cell or ambient temperature (°C)
- **dod_ah** (nx1 double): Depth of discharge (Ah)
- **soc** (nx1 double): State of charge (%)

### EIS

**DATTES eis** structure contains measurements for each EIS test. Data is organised into cells (**mx1** for **m** EIS measurements).
Generally, an **eis** structure contains the following fields:
- **t** (mx1 cell): test time in seconds from 1/1/2000.
- **U** (mx1 cell): cell voltage (V)
- **I** (mx1 cell): cell current (A)
- **m** (mx1 cell): cycler mode (1 = CC, 2=CV, 3 = rest, 4 = EIS, 5 = random profile)
- **ReZ** (mx1 cell): real part of impedance (Ohm)
- **ImZ** (mx1 cell): imaginary part of impedance (Ohm)
- **f** (mx1 cell): frequency (Hz)
- **Iavg** (mx1 cell): Average current (A) in GEIS
- **Iamp** (mx1 cell): Current amplitude (A) in GEIS


### Phases
(TODO)
###  Metadata
(TODO)
###  Configuration
(TODO)

## Options
With **dattes_export** you can export your results (or a part of them) to **.csv**, **.json**, etc.
Several ways are available:
- Export **result struct**: `dattes_export(dattes_struct,options,dst_folder,file_out)`
- Export **mat file**: `dattes_export(file_in,options,dst_folder,file_out)`
- Export **mat file list**: `dattes_export(file_list,options,dst_folder,file_out)`
- Export **mat files in folder**: `dattes_export(src_folder,options,dst_folder,file_out)`

First letter of **options** defines which part of the results will be exported:
- **'A'** = **A**ll : export all the data
- **'P'** = **P**rofiles : export the profiles
- **'E'** = **E**IS : export the EIS
- **'p'** = **p**hases : export the phases
- **'M'** = **M**etadata : export the metadata
- **'C'** = **C**onfiguration : export the configuration

Second letter of **options** defines the target file type:
- **'j'** = export to **json**
- **'c'** = export to **csv** (not available for metadata and configuration
- **'s'** = export to M-file **script** (just available for metadata and configuration

Finally, a third letter can be added to **options**:
- **'m'** = include metadata in profiles,eis, and phases export

## file_out
(TODO)


