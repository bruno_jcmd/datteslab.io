---
title: Learn the DATTES workflow
subtitle: 
comments: false
---

## The DATTES workflow in 4 functions
DATTES aims to transform the raw experimental data into valuable processed results that can be used for visualisation, modeling and/or prediction.
To reach this goal, four main functions have been developed :

1. **dattes_import**: the first function of the workflow consist in transforming the raw experimental data produced in a proprietary format by the cycler into a standard and open format.
The chosen export file format is **.xml** file with a [VEHLIB](https://gitlab.univ-eiffel.fr/eco7/vehlib) structure.  Find more details regarding **dattes_import**, [**this page**](/page/documentation/import_files).

2. **dattes_structure**: thanks to this the second function **.xml** files are interpreted and transformed into a DATTES structure. The main operations at this step are: enrich exprimental data with metadata, calculate state of charge and segment the experiment into phases. Further reading about **dattes_structure** is available at [**this page**](/page/documentation/structure_files).

3. **dattes_configure**: with this third function, DATTES find the suitable experiment phases for each possible analysis. The default analysis methodology may be customized by the user at this step.  Further reading about **dattes_configure** is available at [**this page**](/page/documentation/configuration_files)

4. **dattes_analyse**: the last step is the featurization one. It consists in analysing the data to obtain key performance indicators of tha energy storage system e.g. capacity, equivalent resistance, impedance identification, open circuit voltage or incremental capacity. Further reading about **dattes_analyse** is available at [**this page**](/page/documentation/dattes_analyse).
