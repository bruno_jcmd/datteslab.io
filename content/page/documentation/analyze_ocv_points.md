 --- 
title : Open Circuit Voltage (OCV)
---
# Outline
-[**pseudo-OCV**](#pseudo-ocv)
	- [**Code for analysis**](#code-for-analysis)
	- [**Code for visualization**](#code-for-visualization)
	- [**Methodology and hypothesis**](#methodology-and-hypothesis)
	- [**Contribute to ocv by points analysis**](#contribute-to-capacity-analysis)
-[**OCV by points**](#ocv-by-points)
	- [**Code for analysis**](#code-for-analysis)
	- [**Code for visualization**](#code-for-visualization)
	- [**Methodology and hypothesis**](#methodology-and-hypothesis)
	- [**Contribute to ocv by points analysis**](#contribute-to-capacity-analysis)
	
# Pseudo-OCV	


## Code for analysis

To analyze the resistance, the action 'P' should be used :

`[result] = dattes(XML_file,'Pvs');`

The output are :

| Output structure | Field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |:--- |
| result |  pseudo_ocv|  `pOCV`| V |Voltage during the pseudo ocv test|
| result | pseudo_ocv |  `pDoD`| Ah|Depth of discharge during the pseudo ocv test|
| result | pseudo_ocv |  `pPol`| V |Voltage polarization during the pseudo ocv test|
| result | pseudo_ocv | `pEff` | %| Efficiency during the pseudo ocv test|
| result | pseudo_ocv | `UCi` | V|Charge voltage during the pseudo ocv test|
| result | pseudo_ocv | `UDi` | V|Discharge voltage during the pseudo ocv test|
| result | pseudo_ocv | `Regime` | -|Current rate during the pseudo ocv test|


## Code for vizualization
To visualize the pseudo ocv, the action 'GP' should be used :

`[result] = dattes(XML_file,'GP');`

The graph should look like

![image](/images/figure_GP.png)


## Methodology and Hypothesis
### Method
The `pOCV`, `pPol` and `pEff` are calculated as combination of the voltage profiles during charge and discharge :
```
pOCV = cellfun(@(x,y) (x+y)/2,UCi,UDi,'uniformoutput',false);
pPol = cellfun(@(x,y) (x-y),UCi,UDi,'uniformoutput',false);
pEff = cellfun(@(x,y) (y./x),UCi,UDi,'uniformoutput',false);
```
with UCi and UDi which are respectively the linear interpolation of the voltage during charge and discharge :

```
UCi = cellfun(@(x,y) interp1(x,y,pDoD),DoDAhCs,UCs,'uniformoutput',false);
UDi = cellfun(@(x,y) interp1(x,y,pDoD),DoDAhDs,UDs,'uniformoutput',false);
```

This linear interpolation is made over pDoD vector which contains the coordinates of the query points. It is defined by the user in the configuration file  :

` pDoD = (0:config.dQOCV:config.Capa)';`



### Key parameters for the calculation
The key parameters for the calculation of the pseudo OCV are the quantities that make it possible to define what a pseudo-OCV phase is.

 This definition is made in the configurator function  thanks to the following parameters : 
 
 - config.pOCVpC,
 - config.pOCVpD,
 - config.dQOCV,
 - config.Capa.


### Assumptions and possible simplifications
No major assumptions or simplifications have been made

## Contribute to pseudo ocv analysis
A list of open issues related to pseudo ocv calculation and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues/?search=ocv&sort=created_date&state=opened&first_page_size=20).


	
	
	
	
	
	

# OCV by points

## Code for analysis

To analyze the resistance, the action 'O' should be used :

`[result] = dattes(XML_file,'Ovs');`

The output are :

| Output structure | Field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |:--- |
| result |  ocv_points|  `ocv`| V | Open circuit voltage by points|
| result | ocv_points |  `dod`| Ah|Depth of discharge|
| result | ocv_points | `time` | s | Time of open circuit voltage  measurement|
| result | ocv_points | `sign` | +/-|Sign of the current |

## Code for visualization
To visualize the ocv by points, the action 'GO' should be used :

`[result] = dattes(XML_file,'GO');`

The graph should look like

![image](/images/figure_GO.png)




## Methodology and Hypothesis
### Method
All the array from `result.ocv_points` are determined thanks to function `extract_phase` : 
```
phasesOCV = phases(config.pOCVr);

for ind = 1:length(phasesOCV)
    [tp,Up,DoDAhp] = extract_phase(phasesOCV(ind),t,U,DoDAh);
    tOCVp(ind) = tp(end);
    OCVp(ind) = Up(end);
    DoDp(ind) = DoDAhp(end);
    Ipsign(ind) = sign(phasesAvant(ind).Iavg);
end
```

### Key parameters for the calculation
The key parameter for the calculation of the OCV by point is `pOCVr` as it is the quantity that make possible to define what an OCV by point phase is.


### Assumptions and possible simplifications
No major assumptions or simplifications have been made

## Contribute to ocv by points analysis
A list of open issues related to ocv by points calculation and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues/?search=ocv&sort=created_date&state=opened&first_page_size=20).




