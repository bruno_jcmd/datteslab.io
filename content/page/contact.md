---
title: Contact
subtitle: How to report bug, request new features, ask for help, start a collaboration ?
comments: false
---
# Contact Us

There are a few ways to get in touch, whether it is for reporting bugs, requesting new features, asking for help, or for starting a collaboration !

## Bug reports and feature requests
To submit bug reports or feature requests, go to our [gitlab issues page](https://gitlab.com/dattes/dattes/-/issues). We always appreciate these contributions and feedback ! 

For development discussions and to get help using DATTES, you can email dattes@univ-eiffel.fr.

## Collaborations

If you are interested in collaborating, either in an academic or industrial setting, you can contact us by emailing dattes@univ-eiffel.fr

Academic collaborations take the form of co-authorship of a journal paper, while industrial collaborations can be either consultancy or funding of a research project.
