---
title : DATTES structure 
subtitle : How DATTES is built ?
---



# DATTES architecture
This section will describe the architecture of DATTES.

More to come ...

# Focus on key functions
This section will describe in detail some key functions of DATTES.

## ident_capacity

### 1-Definition of capacity
The capacity  of a cell, expressed in Ampere-hours (Ah), is the amount of charge the cell can deliver at a given constant current. It varies according to the electrode materials and the internal design of the cell. It tends to decrease during the life of the cell. [Source: [Romain Mathieu 2020](https://tel.archives-ouvertes.fr/tel-02920329)]

### 2-Structure in DATTES
![capacite.svg](uploads/11504d2fda316004820a53ecc66a07c5/capacite.svg)
To be replaced by a more relevant tree structure

### 3- Method
The capacity is calculated by summing the capacity calculated during the DC phase with that of a possible CV phase.

The **capacity in the CC phase** is calculated by the ident_Capa2 function with the following code:

```
CapaCC = abs([phases(config.pCapaD | config.pCapaC).capa]);
Regime = [phases(config.pCapaD | config.pCapaC).Imoy]./config.Capa;
TimeFinCC= [phases(config.pCapaD | config.pCapaC).tFin];
```

The **capacity in phase CV** is calculated by the function ident_CapaCV with the following code:

```
CapaCV = abs([phases(config.pCapaDV | config.pCapaCV).capa]);
dCV = [phasesCV.duration];
TimeFinCV= [phases(config.pCapaDV | config.pCapaCV).tFin];
```

The capacity of a cell is calculated by adding the **sum of CapaCC and CapaCV**.
   
```
    for ind=1:length(CapaCC)
      if ind<=length(CapaCV)
         result.Capa(ind)=CapaCC(ind)+CapaCV(ind);
      else
          resultat.Capa(ind)=CapaCC(ind);
       end
    end
```
### 4-Key quantities for the calculation
The four key quantities for the calculation of the capacity are :
- **config.pCapaD**, 
- **config.pCapaC**,
- **config.pCapaDV**,
- **config.pCapaCV**.

They are determined in configurator2 :
```
%1) phases capa decharge (CC):
%1.1. imoy<0
%1.2 - end at SoC0 (I0cc)
%1.3 - are preceded by a rest phase at SoC100 (I100r or I100ccr)
pCapaD = [phases.Imoy]<0 & ismember(tFins,t(I0cc)) & [0 pRepos100(1:end-1)];
%2) phases capa charge (CC):
%2.1.- Imoy>0
%2.2 - end at SoC100 (I100cc)
%2.3 - are preceded by a rest phase at SoC0 (I0r or I0ccr)
pCapaC = [phases.Imoy]>0 & ismember(tFins,t(I100cc)) & [0 pRepos0(1:end-1)];
% pCapaC = [phases.Imoy]>0 & [0 pRepos0(1:end-1)];%LYP, BRICOLE
%3) residual load phases
%1.1.- Imoy<0
%1.2 - end at SoC0 (I0)
%1.3 - are preceded by a phase pCapaD
pCapaDV = [phases.Imoy]<0 & ismember(tFins,t(I0)) & [0 pCapaD(1:end-1)];
%4) residual load phases
%1.1 - Imoy>0
%1.2 - end at SoC100 (I100)
%1.3 - are preceded by a phase pCapaC
pCapaCV = [phases.Imoy]>0 & ismember(tFins,t(I100)) & [0 pCapaC(1:end-1)];
```


### 5-Assumptions and possible simplifications
No major assumptions or simplifications have been made




