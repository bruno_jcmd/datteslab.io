---
title: DATTES is out !
date: 2022-06-06
---

Good news! The first official release of DATTES is now out !

After finalizing some improvement on code quality including checking coding style, reviewing code documentation and unit testing of all general functionalities, we have decided to release the first DATTES version.

This DATTES version works perfectly fine under MATLAB/linux environment and have been used in [this recent publication](https://www.researchgate.net/publication/361309990_Second_Life_Batteries_in_a_Mobile_Charging_Station_Model_Based_Performance_Assessment).

In a MATLAB/windows or in an Octave environment, you can already use most DATTES features including :
- import of bitrode or biologic files
- SoC and capacity calculations
- OCV measurements (pseudo-OCV + OCV by points)
- impedance identification
- etc.

Some bugs are being fixed regarding Arbin/Excel files import.

If you can't wait to discover future DATTES features, please check this [link to next release](https://gitlab.com/dattes/dattes.gitlab.io/-/releases) to be available soon (by end of June).

For those people willing to help us to test or those eager to have access to DATTES, you can clone the repository in your computer to have the development version by using git.
