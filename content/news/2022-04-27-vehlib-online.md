---
title : VEHLIB is open-source
date: 2022-04-27
---
VEHLIB software is another software developped by the Eco7 team.

It is an hybrid vehicle simulation tool based on a block diagram description associated with principles from Bond-Graph theory.

A Backward approach proposes energy management strategy based on DP and PMP principles.

VEHLIB is available on gitlab : https://gitlab.univ-eiffel.fr/eco7/vehlib
